---
layout: post
title:  "Python Flask HTTP Server Tutorial"
date:   2021-03-21 11:31:00 +0000
categories: Python Flask
tags: ['Python', 'Flask', 'HTTP', 'Backend']
thumbnail: "https://cdn.pixabay.com/photo/2017/03/20/21/00/server-2160321_960_720.jpg"
---

<img class="nohover" src="https://cdn.pixabay.com/photo/2017/02/01/11/17/alien-2029727_960_720.png" width="100px">
# Step1. Beginner

## Flask 프레임워크에 대해서
![](https://flask.palletsprojects.com/en/1.1.x/_images/flask-logo.png)

> 플라스크(Flask)는 파이썬으로 작성된 마이크로 웹 프레임워크의 하나로, Werkzeug 툴킷과 Jinja2 템플릿 엔진에 기반을 둔다. BSD 라이선스이다.
> <플라스크 (웹 프레임워크)>

Flask frameworks는 Microservice 제작에 유용하게 활용되는 HTTP 서버 frameworks 입니다.

## venv 환경 설정
[Python3 공식 문서 (가상 환경 및 패키지)](https://docs.python.org/ko/3/tutorial/venv.html)

이번 프로젝트만을 위한 파이썬 개발 환경을 만들어 보겠습니다.

```bash
$ python3 -m venv flask-tutorial
$ cd flask-tutorial
$ source ./bin/activate
```

## Python3, Flask 설치

```bash
$ pip install Flask
```

## Hello, World

Flask의 Hello, World 버전 소스코드 입니다.

```python
from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run()
```

<img class="nohover" src="https://cdn.pixabay.com/photo/2012/04/02/14/27/bee-24638_960_720.png" width="100px">
# Step2. Amateur

<img class="nohover" src="https://cdn.pixabay.com/photo/2017/02/01/11/51/abstract-2029897_960_720.png" width="100px">
# Step3. Professional
